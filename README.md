# AuthChoise Widget Extension

## Installation:

```
composer require credy/yii2-authchoiceremember:"^1.0"
```


## Configuration:
### LogoutBehavior:

Put following snippet into your config

```php
return [
    'components' => [
        'user' => [
            'identityClass' => \common\models\User::class,
            'enableAutoLogin' => true,
            'as logoutBehavior' => credy\authchoiceremember\behaviors\LogoutBehavior::class,
        ]
    ]
];
```

### AuthAction:

Put following snippet into your config

```php

return [
    'components' => [
        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'google' => [
                    'class' => yii\authclient\clients\Google::class,
                    'parametersToKeepInReturnUrl' => [
                        'authclient',
                        'rememberMe',
                        'duration',
                    ],
                ]
            ]
        ],
    ],
];
```

## Usage:

In your user model:
```php
class User extends ActiveRecord implements IdentityInterface
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'logoutIdentityBehavior' => [
                'class' => LogoutIdentityBehavior::class,
                'callback' => [$this, 'changeAuthKey']
            ]
        ]);
    }

    public function changeAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();

        $this->save();
    }
}
```


In your view:
```php

// in your view

echo credy\authchoiceremember\AuthChoice::widget([
    'baseAuthUrl' => ['site/auth'],
    'popupMode' => false,
    'askRememberMe' => true,
    'loginDuration' => 3600 * 24 * 14,
])
```
