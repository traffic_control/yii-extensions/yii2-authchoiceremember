<?php

namespace credy\authchoiceremember\behaviors;

use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\web\User;

class LogoutBehavior extends Behavior
{
    const EVENT_AFTER_LOGOUT = 'afterLogout';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            User::EVENT_AFTER_LOGOUT => 'afterLogout',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        if (!($owner instanceof User)) {
            throw new InvalidConfigException('$owner must be instance of ' . User::class);
        }
        parent::attach($owner);
    }

    /**
     * @inheritdoc
     */
    public function afterLogout($event)
    {
        $event->identity->trigger(self::EVENT_AFTER_LOGOUT, $event);
    }
}
