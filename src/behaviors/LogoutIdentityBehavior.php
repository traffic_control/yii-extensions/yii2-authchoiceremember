<?php

namespace credy\authchoiceremember\behaviors;

use yii\base\Behavior;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\IdentityInterface;

class LogoutIdentityBehavior extends Behavior
{
    public $callback;

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            LogoutBehavior::EVENT_AFTER_LOGOUT => 'afterLogout',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attach($owner)
    {
        if (!($owner instanceof Component)) {
            throw new InvalidConfigException('$owner must be instance of ' . Component::class);
        }
        if (!($owner instanceof IdentityInterface)) {
            throw new InvalidConfigException('$owner must implement ' . IdentityInterface::class);
        }
        parent::attach($owner);
    }

    /**
     * @inheritdoc
     */
    public function afterLogout()
    {
        call_user_func($this->callback);
    }
}
