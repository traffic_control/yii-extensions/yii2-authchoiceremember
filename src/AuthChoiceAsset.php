<?php

namespace credy\authchoiceremember;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class AuthChoiceAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js = [
        'js/auth-choice.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        JqueryAsset::class
    ];
}
