var AuthChoiceRememberWidget = {
    updateAuthLinks: function (element) {
        $(element).parent().parent().children('.auth-clients').children().each(function (key, child) {
            var authUrl = new URL($(this).children().prop('href'));
            authUrl.searchParams.set('rememberMe', $(element).is(':checked') ? '1' : '0');
            $(this).children().attr('href', authUrl.href);
        })
    }
}