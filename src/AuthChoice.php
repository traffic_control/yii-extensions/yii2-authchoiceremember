<?php

namespace credy\authchoiceremember;

use Yii;
use yii\authclient\widgets\AuthChoice as BaseAuthChoice;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class AuthChoice
 *
 * @package credy\authchoiceremember
 */
class AuthChoice extends BaseAuthChoice
{
    /**
     * @var boolean
     */
    public $askRememberMe = false;

    /**
     * @var boolean
     */
    public $rememberMe = false;

    /**
     * @var int
     */
    public $loginDuration = 0;

    /**
     * Runs the widget.
     *
     * @return string rendered HTML.
     */
    public function run()
    {
        $content = '';
        if ($this->autoRender) {
            $content .= $this->renderMainContent();
        }

        if ($this->askRememberMe) {
            $content .= $this->renderCheckBox();
        }

        $content .= Html::endTag('div');
        return $content;
    }

    /**
     * Composes client auth URL.
     *
     * @param ClientInterface $client external auth client instance.
     *
     * @return string auth URL.
     */
    public function createClientUrl($client)
    {
        $this->autoRender = false;
        $url = $this->getBaseAuthUrl();
        $url[$this->clientIdGetParamName] = $client->getId();

        $url['duration'] = $this->loginDuration;
        $url['rememberMe'] = $this->rememberMe;

        return Url::to($url);
    }

    protected function renderCheckBox()
    {
        $view = $this->getView();
        AuthChoiceAsset::register($view);

        return Html::checkbox('rememberMe', false, [
            'label' => Yii::t('app', 'Remember Me'),
            'onchange' => 'AuthChoiceRememberWidget.updateAuthLinks(this)',
        ]);
    }
}
