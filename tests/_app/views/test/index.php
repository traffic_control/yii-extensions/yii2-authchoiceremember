<?php

use credy\authchoiceremember\AuthChoice;

?>

<?= AuthChoice::widget([
    'id' => 'test-widget',
    'askRememberMe' => true,
]); ?>

<?= AuthChoice::widget([
    'id' => 'test2-widget',
    'askRememberMe' => true,
]);
