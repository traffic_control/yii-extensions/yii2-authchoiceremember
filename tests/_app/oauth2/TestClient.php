<?php

namespace yiiacceptance\oauth2;

use yii\authclient\OAuth2;

class TestClient extends OAuth2
{
    protected function initUserAttributes()
    {
        return [];
    }
}
