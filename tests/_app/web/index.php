<?php

require(__DIR__ . '/../../../vendor/autoload.php');
require(__DIR__ . '/../../../vendor/yiisoft/yii2/Yii.php');

Yii::setAlias('yiiacceptance', dirname(__DIR__));

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

// load application configuration
$config = [
    'id' => 'test',
    'basePath' => dirname(__DIR__),
    'defaultRoute' => 'test/index',
    'aliases' => [
        '@vendor'   => dirname(dirname(dirname(__DIR__))) . '/vendor',
        '@bower'    => '@vendor/bower-asset',
        '@npm'      => '@vendor/npm-asset',
        '@runtime'  => dirname(dirname(__DIR__)) . '/_output/runtime',
    ],
    'bootstrap' => ['log'],
    'controllerNamespace' => 'yiiacceptance\controllers',
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'test' => [
                    'class' => yiiacceptance\oauth2\TestClient::class,
                ],
                'test2' => [
                    'class' => yiiacceptance\oauth2\TestClient::class,
                ]
            ]
        ],
        'request' => [
            'enableCookieValidation' => false
        ]
    ]
];

// instantiate and configure the application
(new yii\web\Application($config))->run();
