<?php

namespace tests\acceptance;

use tests\AcceptanceTester;

class RememberMeCest
{
    public function testRememberMeCheckbox(AcceptanceTester $I)
    {
        $I->amOnPage('?r=test');

        $I->seeElement('#test-widget');

        $I->seeElement('a.test-client[title="TestClient"]');
        $I->seeElement('#test-widget input[type="checkbox"][name="rememberMe"]');

        $I->dontSeeCheckboxIsChecked('#test-widget input[type="checkbox"][name="rememberMe"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=0"][href*="authclient=test"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=0"][href*="authclient=test2"]');
        $I->seeElement('#test2-widget a.test-client[href*="rememberMe=0"][href*="authclient=test"]');
        $I->seeElement('#test2-widget a.test-client[href*="rememberMe=0"][href*="authclient=test2"]');


        $I->checkOption('#test-widget input[type="checkbox"][name="rememberMe"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=1"][href*="authclient=test"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=1"][href*="authclient=test2"]');
        $I->seeElement('#test2-widget a.test-client[href*="rememberMe=0"][href*="authclient=test"]');
        $I->seeElement('#test2-widget a.test-client[href*="rememberMe=0"][href*="authclient=test2"]');


        $I->uncheckOption('#test-widget input[type="checkbox"][name="rememberMe"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=0"][href*="authclient=test"]');
        $I->seeElement('#test-widget a.test-client[href*="rememberMe=0"][href*="authclient=test2"]');
    }
}
