<?php

define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);
require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../vendor/autoload.php';

Yii::setAlias('@tests', __DIR__);

$config = [
    'id' => 'app-test',
    'basePath' => dirname(__DIR__),
    'extensions' => require dirname(__DIR__) . '/vendor/yiisoft/extensions.php',
    'components' => [
        'authClientCollection' => [
            'class' => yii\authclient\Collection::class,
            'clients' => [
                'google' => [
                    'class' => yii\authclient\clients\Google::class,
                    'parametersToKeepInReturnUrl' => [
                        'authclient',
                        'rememberMe',
                        'duration',
                    ],
                ]
            ]
        ],
    ],
];

// instantiate and configure the application
(new yii\web\Application($config));

Yii::setAlias('@bower', dirname(__DIR__) . '/vendor/bower-asset');
