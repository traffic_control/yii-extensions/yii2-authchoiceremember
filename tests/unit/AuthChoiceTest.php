<?php

namespace tests\unit;

use Codeception\Test\Unit;
use credy\authchoiceremember\AuthChoice;
use credy\authchoiceremember\AuthChoiceAsset;
use tests\unit\controllers\SiteController;
use Yii;
use yii\base\Module;
use yii\web\AssetManager;
use yii\web\View;

class AuthChoiceTest extends Unit
{
    public function testRun()
    {
        $view = new View([
            'assetManager' => new AssetManager([
                'basePath' => codecept_output_dir(),
                'baseUrl' => codecept_output_dir(),
            ])
        ]);

        Yii::$app->setComponents([
            'view' => $view
        ]);
        Yii::$app->controller = new SiteController('test-controller', new Module('test-module'));

        $widget = new AuthChoice([
            'id' => 'testauthchoice',
            'baseAuthUrl' => ['site/auth'],
            'popupMode' => false,
            'askRememberMe' => true,
            'loginDuration' => 3600 * 24 * 14,
        ]);
        $authChoiceHtml = $widget->run();

        $this->stringContains('rememberMe=0', $authChoiceHtml);
        $this->stringContains('duration=1209600', $authChoiceHtml);
        $this->stringContains('AuthChoiceRememberWidget.updateAuthLinks(&quot;testauthchoice&quot;)', $authChoiceHtml);
    }

    public function testAssetRegistration()
    {
        $view = new View([
            'assetManager' => new AssetManager([
                'basePath' => codecept_output_dir(),
                'baseUrl' => codecept_output_dir(),
            ])
        ]);

        Yii::$app->setComponents([
            'view' => $view
        ]);
        Yii::$app->controller = new SiteController('test-controller', new Module('test-module'));

        $widget = new AuthChoice([
            'baseAuthUrl' => ['site/auth'],
            'popupMode' => false,
            'askRememberMe' => true,
            'loginDuration' => 3600 * 24 * 14,
        ]);

        $widget->run();

        $this->assertArrayHasKey(AuthChoiceAsset::class, $view->assetBundles);
    }
}
