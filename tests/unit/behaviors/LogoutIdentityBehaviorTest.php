<?php

namespace tests\unit\behaviors;

use Codeception\Test\Unit;
use credy\authchoiceremember\behaviors\LogoutBehavior;
use credy\authchoiceremember\behaviors\LogoutIdentityBehavior;
use yii\base\BaseObject;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\IdentityInterface;

class LogoutIdentityBehaviorTest extends Unit
{
    public function testAfterLogout()
    {
        $calledCount = 0;

        $function = function () use (&$calledCount) {
            $calledCount++;
        };

        $identityClass = new class ($function) extends Component implements IdentityInterface
        {
            private $_callback;

            /**
             * @param Unit $test
             * @param array $config name-value pairs that will be used to initialize the object properties
             */
            public function __construct($function, $config = [])
            {
                $this->_callback = $function;
                parent::__construct($config);
            }

            public function testFunction()
            {
                call_user_func($this->_callback);
            }

            public static function findIdentity($id)
            {
            }

            public static function findIdentityByAccessToken($token, $type = null)
            {
            }

            public function getId()
            {
            }

            public function getAuthKey()
            {
            }

            public function validateAuthKey($authKey)
            {
            }
        };

        $behavior = new LogoutIdentityBehavior([
            'callback' => [$identityClass, 'testFunction']
        ]);

        $identityClass->attachBehavior('logoutIdentityBehavior', $behavior);
        $identityClass->trigger(LogoutBehavior::EVENT_AFTER_LOGOUT);

        $this->assertEquals(1, $calledCount);
    }

    public function testAttachToNotIdentityInterface()
    {
        $identityClass = new Component();

        $behavior = new LogoutIdentityBehavior([
            'callback' => [$identityClass, 'testFunction']
        ]);

        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('$owner must implement ' . IdentityInterface::class);

        $identityClass->attachBehavior('logoutIdentityBehavior', $behavior);
    }

    public function testAttachToNotComponent()
    {
        $identityClass = new BaseObject();

        $behavior = new LogoutIdentityBehavior([
            'callback' => [$identityClass, 'testFunction']
        ]);

        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('$owner must be instance of ' . Component::class);

        $behavior->attach($identityClass);
    }
}
