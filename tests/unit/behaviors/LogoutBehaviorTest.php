<?php

namespace tests\unit\behaviors;

use Codeception\Test\Unit;
use credy\authchoiceremember\behaviors\LogoutBehavior;
use yii\base\Component;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\web\IdentityInterface;
use yii\web\User;
use yii\web\UserEvent;

class LogoutBehaviorTest extends Unit
{
    public function testAfterLogout()
    {
        $behavior = new LogoutBehavior();

        $calledCount = 0;

        $function = function ($name) use (&$calledCount) {
            $calledCount++;
            $this->assertEquals(LogoutBehavior::EVENT_AFTER_LOGOUT, $name);
        };

        $identityClass = new class ($function) extends Component implements IdentityInterface
        {
            private $_callback;

            /**
             * @param Unit $test
             * @param array $config name-value pairs that will be used to initialize the object properties
             */
            public function __construct($function, $config = [])
            {
                $this->_callback = $function;
                parent::__construct($config);
            }

            public function trigger($name, ?Event $event = null)
            {
                call_user_func($this->_callback, $name);
            }
            public static function findIdentity($id)
            {
            }

            public static function findIdentityByAccessToken($token, $type = null)
            {
            }

            public function getId()
            {
            }

            public function getAuthKey()
            {
            }

            public function validateAuthKey($authKey)
            {
            }
        };
        $user = new User(['identityClass' => $identityClass]);

        $user->attachBehavior('logoutBehavior', $behavior);

        $user->trigger(User::EVENT_AFTER_LOGOUT, new UserEvent([
            'identity' => $identityClass
        ]));

        $this->assertEquals(1, $calledCount);
    }

    public function testAttachingToNotYiiWebUser()
    {
        $behavior = new LogoutBehavior();

        $user = new Component();

        $this->expectException(InvalidConfigException::class);
        $this->expectExceptionMessage('$owner must be instance of ' . User::class);
        $user->attachBehavior('logoutBehavior', $behavior);
    }
}
