<?php

namespace tests\unit\controllers;

use yii\authclient\AuthAction;
use yii\web\Controller as WebController;

/**
 * Site controller
 */
class SiteController extends WebController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => AuthAction::class,
            ],
        ];
    }
}
